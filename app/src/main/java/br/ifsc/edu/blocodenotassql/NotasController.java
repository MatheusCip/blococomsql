package br.ifsc.edu.blocodenotassql;

import android.content.Context;

public class NotasController {

    private NotasDAO notasDAO;

    public NotasController(Context context) {
        notasDAO = new NotasDAO(context);
    }

    public void salvaNota(Notas notas) {
        notasDAO.salvaNota(notas);
    }

    public String recuperarTexto() {
        return notasDAO.recuperarTexto();
    }

    public boolean idExistente() {
        return notasDAO.idExistente();
    }

    public void updateTexto(Notas notas) {
        notasDAO.updateTexto(notas);
    }
}
