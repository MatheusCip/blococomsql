package br.ifsc.edu.blocodenotassql;

public class Notas {

    private int id;
    private String texto;

    public Notas(int id, String texto) {
        this.id = id;
        this.texto = texto;
    }

    public int getId() {
        return id;
    }

    public String getTexto() {
        return texto;
    }
}
