package br.ifsc.edu.blocodenotassql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class NotasDAO {

    private static final String NOME_BANCO = "Banco";
    private SQLiteDatabase db;

    public NotasDAO(Context context) {
        this.db = context.openOrCreateDatabase(NOME_BANCO, Context.MODE_PRIVATE, null);
        criaTabelaNota();
    }

    public void criaTabelaNota() {
        db.execSQL("CREATE TABLE IF NOT EXISTS notas " +
                "(not_id INTEGER PRIMARY KEY NOT NULL, " +
                "not_texto TEXT NOT NULL);");
    }


    public void salvaNota(Notas notas) {
        ContentValues contentValues =  new ContentValues();
        contentValues.put("not_id", notas.getId());
        contentValues.put("not_texto", notas.getTexto());
        db.insert("notas", null, contentValues);
    }

    public boolean idExistente() {
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM notas WHERE not_id=1", null, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            String texto = cursor.getString(cursor.getColumnIndex("not_texto"));
            return true;
        } else {
            return false;
        }
    }

    public void updateTexto(Notas notas) {
        db.execSQL("UPDATE notas SET not_texto = '"+ notas.getTexto() +"' WHERE not_id = 1");
    }

    public String recuperarTexto() {
        Cursor cursor;
        cursor = db.rawQuery("SELECT not_texto FROM notas WHERE not_id = 1", null, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            String texto = cursor.getString(cursor.getColumnIndex("not_texto"));
            return texto;
        } else {
            return null;
        }
    }
}
