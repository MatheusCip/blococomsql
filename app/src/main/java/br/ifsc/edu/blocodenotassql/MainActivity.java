package br.ifsc.edu.blocodenotassql;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private EditText nota;
    private TextView texto;
    private NotasController notasController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.nota = findViewById(R.id.editText2);
        this.texto = findViewById(R.id.textView);
        notasController = new NotasController(MainActivity.this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!nota.getText().toString().trim().isEmpty()) {
                    Notas notas = new Notas(1, nota.getText().toString());
                    if (!notasController.idExistente()) {
                        notasController.salvaNota(notas);
                    } else {
                        notasController.updateTexto(notas);
                    }
                    texto.setText(nota.getText().toString());
                } else {
                    Toast.makeText(MainActivity.this, "Não foi possivel salvar a nota", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        texto.setText(notasController.recuperarTexto());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Notas notas = new Notas(1, nota.getText().toString());
        if (!notasController.idExistente()) {
            notasController.salvaNota(notas);
        } else {
            notasController.updateTexto(notas);
        }
    }
}
